package myhomework6.Interfaces;

import myhomework6.myhomework5Classes.Human;
import myhomework6.myhomework5Classes.Man;

import java.util.ArrayList;
import java.util.List;

public interface HumanCreator {
    Human bornChild();
}
