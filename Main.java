package myhomework6;

import myhomework6.myhomework5Classes.*;
import myhomework6.myhomework5Enums.DayOfWeek;
import myhomework6.myhomework5Enums.Species;

import java.lang.ref.SoftReference;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //first family
        Man john = new Man("John","Wick",1980);
        john.setIq(150);
        String[][] scheduleJohn = new String[2][2];
        scheduleJohn[0][0] = DayOfWeek.SATURDAY.name();
        scheduleJohn[0][1] = "Play football";
        scheduleJohn[1][0] = DayOfWeek.SUNDAY.name();
        scheduleJohn[1][1] = "Goto the gym";
        john.setSchedule(scheduleJohn);
        Woman karla = new Woman("Karla","Bruni",1980);
        karla.setIq(150);
        String[][] scheduleKarla = new String[2][2];
        scheduleKarla[0][0] = DayOfWeek.SATURDAY.name();
        scheduleKarla[0][1] = "Go shopping";
        scheduleKarla[1][0] = DayOfWeek.SUNDAY.name();
        scheduleKarla[1][1] = "Play tennis";
        karla.setSchedule(scheduleKarla);
        Family familyWick = new Family(karla, john);
        Dog dog = new Dog("Alfa",3,95, new String[]{"play","go for a walk"});
        dog.getSpecies().setNumberOfPaw(4);
        dog.getSpecies().setHasFur(true);
        dog.getSpecies().setCanFly(false);
        familyWick.setPet(dog);
        Woman natali = new Woman("Natali","Portman",1990,97,scheduleKarla);
        familyWick.addChild(natali);
        natali.setFamily(familyWick);
        Woman liza = new Woman("Liza","Mineli",2008,109,scheduleJohn);
        familyWick.addChild(liza);
        liza.setFamily(familyWick);

        //second family
        Man arnold = new Man("Arnold","Schwarzenegger",1960);
        arnold.setIq(200);
        String[][] scheduleArnold = new String[2][2];
        scheduleArnold[0][0] = DayOfWeek.SATURDAY.name();
        scheduleArnold[0][1] = "Go to the gym";
        scheduleArnold[1][0] = DayOfWeek.SUNDAY.name();
        scheduleArnold[1][1] = "Go to the gym";
        arnold.setSchedule(scheduleArnold);
        Woman monica = new Woman("Monica","Belucci",1962);
        monica.setIq(150);
        String[][] scheduleMonica = new String[2][2];
        scheduleMonica[0][0] = DayOfWeek.SATURDAY.name();
        scheduleMonica[0][1] = "Visit Jennifer";
        scheduleMonica[1][0] = DayOfWeek.SUNDAY.name();
        scheduleMonica[1][1] = "Go to the bar";
        monica.setSchedule(scheduleMonica);
        Family familyArnold = new Family(monica, arnold);
        DomesticCat cat = new DomesticCat("Olaf",4,50, new String[]{"sleep","eat"});
        cat.getSpecies().setCanFly(false);
        cat.getSpecies().setHasFur(true);
        cat.getSpecies().setNumberOfPaw(4);
        familyArnold.setPet(cat);
        Woman michel = new Woman("Michel","Schwarzeneger",1992,37,scheduleMonica);
        familyArnold.addChild(michel);
        michel.setFamily(familyArnold);

        System.out.println(familyArnold);
        System.out.println(familyWick);

        //вызов методов ребёнка second family
        System.out.println(michel);
        michel.describePet();
        michel.greetPet();
        michel.feedPet(false);

        //вызов методов Pet
        System.out.println(cat);
        cat.eat();
        cat.respond();

        //вызов метода delete child через human
        System.out.println(Arrays.toString(familyWick.getChildren()));
        familyWick.deleteChild(liza);
        System.out.println(Arrays.toString(familyWick.getChildren()));
        familyArnold.deleteChild(liza);
        cat.foul();
        john.repairCar();
        monica.makeup();
        monica.setFamily(familyArnold );
        Human human = monica.bornChild();
        System.out.println(human);




//        int count = 0;
//        while (count < 10000000) {
//            Man man = new Man();
//        }




    }



}
