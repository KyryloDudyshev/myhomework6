package myhomework6.myhomework5Classes;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }

    public void repairCar() {
        System.out.println("I need to repair my car");
    }

    @Override
    public void greetPet() {
            System.out.println("Привет, " + super.getFamily().getPet().getNickname() + "!");
    }
}
