package myhomework6.myhomework5Classes;

import myhomework6.myhomework5Enums.Species;

public class RoboCat extends Pet {
    Species species = Species.ROBOCAT;

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat() {
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + super.getNickname() + "." + " Я соскучился!");
    }
}
