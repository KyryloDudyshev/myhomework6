package myhomework6.myhomework5Test;

import myhomework6.myhomework5Classes.Family;
import myhomework6.myhomework5Classes.Human;
import myhomework6.myhomework5Classes.Man;
import myhomework6.myhomework5Classes.Woman;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Human john = new Man("John","Wick",1980);
    String[][] scheduleJohn = new String[2][2];
    Human karla = new Woman("Karla","Bruni",1980);
    Family familyWick = new Family(karla, john);
    Family family = new Family(new Man(),new Woman());

    Human natali = new Woman("Natali","Portman",1990);
    Human liza = new Woman("Liza","Mineli",2008,109,scheduleJohn);

    Human father = john;
    Human mother = karla;








    @Test
    void testToString() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        String actual = familyWick.toString();
        String expected = "Family: mother: " + mother.getName() + " " + mother.getSurname() + ", " + "father: " + father.getName() + " " + father.getSurname() + ", children: " + Arrays.toString(children);
        assertEquals(expected,actual);
    }

    @Test
    void testDeleteChildCorrectName() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        familyWick.deleteChild(natali);
        assertEquals(1,familyWick.getChildren().length);
    }

    @Test
    void testDeleteChildInCorrectName() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        Human child = new Man("Alex","Smith",35);
        familyWick.deleteChild(child);
        assertEquals(2,familyWick.getChildren().length);
    }


    @Test
    void testdeleteChildInCorrectIndex() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        familyWick.deleteChild(3);
        assertEquals(2,familyWick.getChildren().length);
    }

    @Test
    void testdeleteChildCorrectIndex() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        familyWick.deleteChild(0);
        assertEquals(1,familyWick.getChildren().length);
    }

    @Test
    void testdeleteChildInCorrectIndexBoolean() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        assertEquals(false,familyWick.deleteChild(3));
    }

    @Test
    void testdeleteChildCorrectIndexBoolean() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        assertEquals(true,familyWick.deleteChild(1));
    }

    @Test
    void testAddChild() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        Human child = new Man("Alex","Smith",35);
        familyWick.addChild(child);
        assertEquals(3,familyWick.getChildren().length);
    }

    @Test
    void testAddChildCheckObject() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        Human child = new Man("Alex","Smith",35);
        familyWick.addChild(child);
        assertEquals(true,child.equals(familyWick.getChildren()[2]));
    }

    @Test
    void testCountFamily() {
        Human[] children = {liza,natali};
        familyWick.setChildren(children);
        assertEquals(4,familyWick.getChildren().length + 2);
    }

    @Test
    void testHashcode(){
        assertTrue(family.hashCode()!=familyWick.hashCode());
    }

    @Test
    void testEqualsFalse(){
        assertFalse(family.equals(familyWick));
    }

    @Test
    void testEqualsTrue(){
        assertTrue(familyWick.equals(familyWick));
    }

    //contract Equals
    @Test
    void testSymmetry() {
        assertEquals(family.equals(familyWick),familyWick.equals(family));
    }

    @Test
    void testTransitivity() {
        Family family2 = new Family(new Man(),new Woman());
        boolean[] test = {familyWick.equals(family),family.equals(family2),familyWick.equals(family2)};
        assertEquals(Arrays.toString(new boolean[]{false,false,false}),Arrays.toString(test));
    }

    @Test
    void testConsistency() {
        boolean firstInvoke = familyWick.equals(family);
        boolean secondInvoke = familyWick.equals(family);
        assertEquals(firstInvoke,secondInvoke);
    }

    @Test
    void testNull() {
        assertEquals(false,familyWick.equals(null));
    }




}