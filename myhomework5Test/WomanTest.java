package myhomework6.myhomework5Test;

import myhomework6.myhomework5Classes.Family;
import myhomework6.myhomework5Classes.Human;
import myhomework6.myhomework5Classes.Man;
import myhomework6.myhomework5Classes.Woman;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WomanTest {
    Woman woman = new Woman("Alexa","Paterson",1980,300,new String[2][2]);
    Man man = new Man("Joshua","Slow",1980,198,new String[2][2]);
    Family slow = new Family(woman,man);




    @Test
    void bornChildCorrectSurname() {
        woman.setFamily(slow);
        man.setFamily(slow);
        assertEquals("Slow",woman.bornChild().getSurname());
    }

    @Test
    void bornChildCorrectIq() {
        woman.setFamily(slow);
        man.setFamily(slow);
        assertEquals(249,woman.bornChild().getIq());
    }
}